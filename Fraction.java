public class Fraction {
    private int numerator;
    private int denominator;

    public  Fraction (int numerator, int denominator) {
        this.numerator = numerator;
        this.denominator = denominator;
    }

    public Fraction(int numerator){
        this.numerator = numerator;
        this.denominator = 1;
    }

    private Fraction(){
        numerator = 0;
        denominator = 1;
    }

    public int getDenominator() {
        return denominator;
    }

    public void setDenominator(int denumerator) {
        this.denominator = denumerator;
    }

    public int getNumerator() {
        return numerator;
    }

    public void setNumerator(int numerator) {
        this.numerator = numerator;
    }

    @Override
    public String toString() {
        return numerator + "/" + denominator;
    }
}
