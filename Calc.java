public class Calc {
    private static int common_denominator(int denominator, int denominator1){
        int new_denominator = denominator * denominator1;
        return new_denominator;
    }

    public static Fraction add(Fraction fraction, Fraction fraction1){
         int new_denominator = common_denominator(fraction.getDenominator(), fraction1.getDenominator());
         int new_numerator = (fraction.getNumerator() * fraction1.getDenominator()) + (fraction1.getNumerator() * fraction.getDenominator());
         Fraction new_fraction = new Fraction(new_numerator, new_denominator);
         return new_fraction;
    }

    public static Fraction subtraction(Fraction fraction, Fraction fraction1) {
        int new_denumerator = common_denominator(fraction.getDenominator(), fraction1.getDenominator());
        int new_numerator = (fraction.getNumerator() * fraction1.getDenominator()) - (fraction1.getNumerator() * fraction.getDenominator());
        Fraction new_fraction = new Fraction(new_numerator, new_denumerator);
        return new_fraction;
    }

    public static Fraction multiplication(Fraction fraction, Fraction fraction1){
        int new_denumerator = fraction.getDenominator() * fraction1.getDenominator();
        int new_numeratot = fraction.getNumerator() * fraction1.getNumerator();
        Fraction new_fraction = new Fraction(new_numeratot, new_denumerator);
        return new_fraction;
    }
    public static Fraction division(Fraction fraction, Fraction fraction1){
        fraction1 = overturn(fraction1);
        Fraction new_fraction = multiplication(fraction, fraction1);
        return new_fraction;
    }

    public static Fraction overturn(Fraction fraction) {
        int number = fraction.getDenominator();
        fraction.setDenominator(fraction.getNumerator());
        fraction.setNumerator(number);
        return fraction;
    }


}
