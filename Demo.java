import java.util.Scanner;

public class Demo {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Введите выражение");
        String expression = scanner.nextLine();
        String[] exp = expression.split(" ");
        String [] array = exp[0].split("/");
        Fraction fraction = new Fraction(Integer.parseInt(array[0]), Integer.parseInt(array[1]));
        String [] array1 = exp[2].split("/");
        Fraction fraction1 = new Fraction(Integer.parseInt(array1[0]), Integer.parseInt(array1[1]));
        switch (exp[1]) {
            case "+":
                System.out.println("Сложение = " + Calc.add(fraction, fraction1));
                break;

            case "-":
                System.out.println("Вычитание = " + Calc.subtraction(fraction, fraction1));
                break;

            case "*":
                System.out.println("Умножение = " + Calc.multiplication(fraction, fraction1));
                break;

            case ":":
                System.out.println("Деление = " + Calc.division(fraction, fraction1));
                break;

            default:
                System.out.println("Никакое решение не подходит");
        }


    }
}
